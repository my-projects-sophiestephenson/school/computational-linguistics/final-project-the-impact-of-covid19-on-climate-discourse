# data-collection

This directory contains file needed to collect and organize my initial data. I ran the files in this order:
1. collect-users.ipynb
2. collect-user-tweets.ipynb
3. organize-tweet-files.ipynb

Filename | Contents
-------- | --------
climate-tweets | Contains the volume of climate-related tweets for each location and time period. Each line in this file contains the timestamp of the tweet, its author, the truncated tweet text, and the label given by the taxonomy of climate terms, all separated by tabs.
figures | Contains graphs which depict statistics about the category and proportion of climate change tweets in each location
users | Contains the lists of users for each location
collect-user-tweets.ipynb | The code to gather tweets the timelines of our user lists
collect-users.ipynb | The code to gather user lists for each location
organize-tweet-files.ipynb | The code to split tweets by date and climate-relatedness and to gather basic statistics about the tweets
README.md | This file

The full lists of tweets gathered for each location, as well as the lists of tweets split by date (before separating climate tweets), have been omitted due to their size. I also omitted files which combine all climate tweets in each time period, again because of their size. 