# CMPU 366 Final Project

This was my final project for Vassar Course CMPU366, Computational Linguistics. I decided
to use the linguistics analysis tools we had learned in the course to look at how Twitter
conversations about climate change have shifted since COVID-19. My final report (`final-report.pdf`)
gives more information about the motivations, methods, and results of the project, and all
relevant files are also included in this repository.

Filename | Contents
-------- | --------
before-after-classification |  Files relevant to the before/after classifier
believer-denier-classification |  Files relevant to the believer/denier classifier
data-collection | Data files and code used to collect data
final-report.pdf | PDF file containing the project report
README.md | This file
sentiment-classification | Files relevant to the sentiment classifier
