# before-after-classification
This directory contains files used for the before/after classifier. 

Name | Contents
-------- | --------
before-after-classification.ipynb | Code for the classifier
conf-matrix.png | A figure depicting the confusion matrix of the classifier
most-informative-feats  | A figure depicting the most informative features of the classifier and their weights
README.md | This file