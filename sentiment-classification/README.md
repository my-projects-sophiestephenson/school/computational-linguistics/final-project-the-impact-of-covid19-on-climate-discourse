# sentiment-classification
This directory contains files used for the sentiment classifier. 

Name | Contents
-------- | --------
conf-matrix.png | A figure depicting the confusion matrix of the classifier
most-informative-feats.png | A figure depicting the most informative features of the classifier
precision-recall-curve.png | A figure depicting the precision-recall curve of the classifier
results-by-location.png | A figure depicting the results of the classifier by showing the percentage of tweets classified as negative in each location and time period
sentiment-classification.ipynb | The code used to train and run the classifier
top-ten-classifiers.png | A table showing the top ten performing classifiers (model and vectorizer pairs) in the comparison stage of classification

The file of positive and negative and tweets used to train the classifier has been omitted here due to size.