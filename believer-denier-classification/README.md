# believer-denier-classification
This directory contains files used for the believer/denier classifier. 

Name | Contents
-------- | --------
believer-denier-classification.ipynb | Code for the classifier
collect-harvard-tweets.ipynb | Code to collect the full climate tweets from the Harvard list of tweet IDs
conf-matrix.png | A figure depicting the confusion matrix of the classifier
definitive_tweets.txt | The list of tweets used to train the classifier. These tweets were "definitive" because they contained either a "believer" hashtag or a "denier" hashtag.
most-informative-feats.png | A figure depicting the most informative features of the classifier and their weights (a positive weight indicates weight towards 'denier')
README.md | This file
results-by-location.png | A figure depicting the results of the classifier by showing the percentage of tweets classified as denier in each location and time period

The lists of Harvard tweet IDs, along with the full lists of collected tweets, have been omitted due to size.